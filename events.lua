-- shock's lmgtfy plugin

local function onTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)
	local channel = ts3.getChannelOfClient(serverConnectionHandlerID, fromID)

	--------------------------
	-- new features 16/02/2016
	--------------------------

	-- ip
	if string.match(message, "!ip") then
	  	print ("Running ip for:", message)
		newmsg = string.gsub( message, "!ip", "" )					
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "http://whatismyipaddress.com/ip/" .. newmsg
		sendMsg = string.gsub( sendMsg, "%+", "" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent ip link.")
	end

	-- whois
	if string.match(message, "!whois") then
	  	print ("Running ip for:", message)
		newmsg = string.gsub( message, "!whois", "" )					
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://whois.icann.org/en/lookup?name=" .. newmsg
		sendMsg = string.gsub( sendMsg, "%+", "" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent whois link.")
	end

	-- tf
	if string.match(message, "!tf") then
	  	print ("Running lmgtfy for:", message)
		newmsg = string.gsub( message, "!tf", "" )					
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://torrentfreak.com/?s=" .. newmsg
		sendMsg = string.gsub( sendMsg, "?s=%+", "?s=" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent tf link.")
	end


	-- erowid
	if string.match(message, "!erowid") then
	  	print ("Running erowid for:", message)
		newmsg = string.gsub( message, "!erowid", "" )					
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://www.erowid.org/search.php?q=" .. newmsg
		sendMsg = string.gsub( sendMsg, "?q=%+", "?q=" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent erowid link.")
	end

	-- google
	if string.match(message, "!google") then
	  	print ("Running lmgtfy for:", message)
		newmsg = string.gsub( message, "!google", "" )					
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://www.google.com/search?q=" .. newmsg
		sendMsg = string.gsub( sendMsg, "?q=%+", "?q=" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent google link.")
	end

	--steam lobby (thanks FaNNboii - https://r4p3.net/members/fannboii.2241/)
	if string.match(message, "steam://[^%s]*") then
		sendMsg = "[url]"..string.match(message, "steam://[^%s]*").."[/url]"
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
	end

	-- lmgtfy
	if string.match(message, "!lmgtfy") then
	  	print ("Running lmgtfy for:", message)
		newmsg = string.gsub( message, "!lmgtfy", "" )	
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "http://lmgtfy.com/?q=" .. newmsg
		sendMsg = string.gsub( sendMsg, "?q=%+", "?q=" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent lmgtfy link.")
	end

	-- r4p3
	if string.match(message, "!r4p3") then
	  	print ("Running r4p3 for:", message)
		newmsg = string.gsub( message, "!r4p3", "" )					
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "http://www.google.com/search?site=&source=hp&q=" .. newmsg:gsub("^+", "") .. "+site%3Ar4p3.net&btnI=1"
		sendMsg = string.gsub( sendMsg, "?q=%+", "?q=" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent r4p3 link.")
	end

	-- tpb
	if string.match(message, "!tpb") then
	  	print ("Running tpb for:", message)
		newmsg = string.gsub( message, "!tpb", "" )					
		newmsg = string.gsub( newmsg, " ", "%%20" )
		sendMsg = "https://tpb.shock.ml/search/" .. newmsg
		sendMsg = string.gsub( sendMsg, "/search/%%20", "/search/" )
		sendMsg = "[url=" .. sendMsg .. "]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent tpb link.")
	end

	-- kat
	if string.match(message, "!kat") then
	  	print ("Running kat for:", message)
		newmsg = string.gsub( message, "!kat", "" )					
		newmsg = string.gsub( newmsg, " ", "" )
		sendMsg = "https://kat.cr/usearch/" .. newmsg
		-- sendMsg = string.gsub( sendMsg, "?q=%+", "?q=" )
		sendMsg = "[url=" .. sendMsg .. "]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent kat link.")
	end

	-- file
	if string.match(message, "!file") then
	  	print ("Running file for:", message)
		newmsg = string.gsub( message, "!file", "" )					
		newmsg = string.gsub( newmsg, " ", "%%20" )
		sendMsg = "http://megasearch.co/?q=" .. newmsg
		sendMsg = string.gsub( sendMsg, "?q=%%20", "?q=" )
		sendMsg = "[url=" .. sendMsg .. "]".. sendMsg .."[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent file link.")
	end

	--------------------
	-- Previous features
	--------------------

	-- youtube
	if string.match(message, "!youtube") then
	  	print ("Running youtube for:", message)
		newmsg = string.gsub( message, "!youtube", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://www.youtube.com/results?search_query=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent youtube link.")
	end

	-- urban dictionary
	if string.match(message, "!ud") then
	  	print ("Running ud for:", message)
		newmsg = string.gsub( message, "!ud", "" )
		newmsg = string.gsub( newmsg, " ", "-" )
		sendMsg = "http://www.urbandictionary.com/define.php?term=" .. newmsg
		sendMsg = string.gsub( sendMsg, "?term=%-", "?term=" )
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent ud link.")
	end

	-- pornhub
	 if string.match(message, "!porn") then
	 	print ("Running pornhub for:", message)
		newmsg = string.gsub( message, "!porn", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "www.pornhub.com/video/search?search=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent porn link.")
	end

	-- soundcloud
	if string.match(message, "!sc") then
	  	print ("Running sc for:", message)
		newmsg = string.gsub( message, "!sc", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://soundcloud.com/search?q=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent sc link.")
	end

	-- wikipedia
	if string.match(message, "!wiki") then
	  	print ("Running wiki for:", message)
		newmsg = string.gsub( message, "!wiki", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://en.wikipedia.org/w/index.php?search=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent wiki link.")
	end

	-- archlinux wiki
	if string.match(message, "!aw") then
	  	print ("Running arch wiki for:", message)
		newmsg = string.gsub( message, "!aw", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://wiki.archlinux.org/index.php?search=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent arch wiki link.")
	end

	-- steam
	if string.match(message, "!steam") then
	  	print ("Running steam for:", message)
		newmsg = string.gsub( message, "!steam", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://steamcommunity.com/search/users/#filter=none&text=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent steam link.")
	end

	-- encyclopedia dramatica
	if string.match(message, "!drama") then
	  	print ("Running drama for:", message)
		newmsg = string.gsub( message, "!drama", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "https://encyclopediadramatica.se/index.php?search=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent drama link.")
	end

	-- youtube search & download
	if string.match(message, "!ytsearch") then
	  	print ("Running ytsearch for:", message)
		newmsg = string.gsub( message, "!ytsearch", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "http://www.google.com/search?site=&source=hp&q=" .. newmsg:gsub("^+", "") .. "+site%3Ayoutube.com&btnI=1"
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent ytsearch link.")
	end
	
	-- google im feeling lucky
	if string.match(message, "!lucky") then
	  	print ("Running lucky for:", message)
		newmsg = string.gsub( message, "!lucky", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "http://www.google.com/search?site=&source=hp&q=" .. newmsg:gsub("^+", "") .. "&btnI=1"
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent lucky link.")
	end

	-- random imgur
	if string.match(message, "!random") then
		print ("Running random pics for:", message)
		sendMsg = "[url]https://rand.pics/[/url]"
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
	end

	-- deadpool
	if string.match(message, "!deadpool") then
		print ("Running random pics for:", message)
		sendMsg = "[color=red][url]http://shock.ml/deadpool/[/url][/color]"
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
	end

	-- hapenis
	if string.match(message, "!hapenis") then
		print ("Running hapenis for:", message)
		sendMsg = "[url]https://encrypted.google.com/#q=hapenis[/url]"
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
	end

	-- rtfw
	if string.match(message, "!rtfw") then
		print ("Running rtfw for:", message)
		sendMsg = "Read the f***ing wiki!"
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
	end

	-- sn
	if string.match(message, "!sn") then
		print ("Running sn for:", message)
		sendMsg = "[url=http://www.specternetworks.com]www.specternetworks.com[/url]"
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
	end

	-- 9gag
	if string.match(message, "!9gag") then
	  	print ("Running 9gag for:", message)
		newmsg = string.gsub( message, "!9gag", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "http://9gag.com/search?query=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent 9gag link.")
	end

	-- imgur
	if string.match(message, "!imgur") then
	  	print ("Running imgur for:", message)
		newmsg = string.gsub( message, "!imgur", "" )
		newmsg = string.gsub( newmsg, " ", "+" )
		sendMsg = "http://imgur.com/search?q=" .. newmsg:gsub("^+", "")
		sendMsg = "[url]" .. sendMsg .. "[/url]"
		print ("Parsed Link: ", sendMsg)
		ts3.requestSendChannelTextMsg(serverConnectionHandlerID, sendMsg, channel)
		print ("Sent imgur link.")
	end
	
end

lmgtfy_events = {
	onTextMessageEvent = onTextMessageEvent
}
